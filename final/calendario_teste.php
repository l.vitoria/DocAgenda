 <?php
	session_start();
	include "conecta.php";
	if(!isset($_SESSION['logado'])){

		echo"<script type='text/javascript'>
		window.alert('voce deve fazer o login');
		location.href='index.php';
		</script>";
				
	}
	 $id_medico=$_SESSION['id_medico'];
	?>
 <form method="post" >
	<input type="submit" class="sair" id="enviar" name="enviar" value="Sair" >
</form>
   <?php
   if(isset($_POST['enviar']) && $_POST['enviar']=="Sair"){
	session_destroy();
	echo "<script>location.href='index.php'</script>";
}
?>

<?php
 
  



   ########################################
   #											
   #		Calendário de Eventos PHP 5 e MySQL			
   #		Documento: Agenda de Eventos Dinâmica		
   #		Autor: Gaspar Teixeira					
   #		E-mail: gaspar.teixeira@gmail.com			
   #		Data: 14/11/2008						
   #		Direito de Uso: Livre						
   #		Declaração: O autor não se responsabiliza		
   #		pelo utilização deste calendário!					
   #												
   ########################################
   
   ?>
   
   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
   <html xmlns="http://www.w3.org/1999/xhtml">
   <head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
   <title>DOC AGENDA</title>
   <style type="text/css">
   body{
   	background-image: url("imagens/calendario.jpg ");
   	background-repeat: no-repeat;
    background-position: 15% 10%;
    background-attachment: fixed;
   }
   .tabela{
    background: #fff;
    width: 200px;
    padding: 0px;
    border: 1px solid #f0f0f0;
    width: 88%;
    margin-top: 2%;
    margin-left: 8%;
    height: 57%;
   }
   .td{
   background:#f8f8f8;
   width:20px;
   height:20px;
   text-align:center;
   
   }
   .hj{
   background: #f8f8f8;
   width:20px;
   height:20px;
   text-align:center;
   }
   .dom{
   background: #B0E0E6;
   width:20px;
   height:20px;
   text-align:center;
   }
   .evt{
   background: #6495ED;
   width:20px;
   height:20px;
   text-align:center;
   }
   .mes{
   background:#fff;
   width:auto;
   height:20px;
   text-align:center;
   }
   .show{
   background:#202020;
   width:300px;
   height:30px;
   text-align:left;
   font-size:12px;
   font-weight:bold;
   color:#CCFF00;
   padding-left:5px;
   }
   .linha{
    background: #6495ed;
    height: 19px;
    text-align: left;
    font-size: 17px;
    color: #f0f0f0;
    padding: 26px 14px 14px 44px;
    width: 50%;
    margin-left: 10%;
   }
   .sem{
   background: #D3D3D3;
   width:auto;
   height:20px;
   text-align:center;
   font-size:12px;
   font-weight:bold;
   font-family:Verdana;
   }
   body,td,th {
	   font-family: Verdana;
	   font-size: 11px;
	   color: #000000;
   }
   a:link {
	   color: #000000;
	   text-decoration: none;
   }
   a:visited {
	   text-decoration: none;
	   color: #000000;
   }
   a:hover {
	   text-decoration: underline;
	   color: #FF9900;
   }
   a:active {
	   text-decoration: none;
   }
   .sair{
  height: 10%; 
  margin: 0 auto;
  width: 30%;
  background-color: #6495ED;
  border: 0;
  border-radius: 3px;
  padding: 0 ;
  color: #fff;
  margin-left: 35%;
  font-size: 30px;
   }
   .mover{
   	margin-left: 35%;
   	width: 50%;
   }
   </style>
   </head>
   
   <body>
   
   <?php 
   include "conecta.php";//conexão com o banco de dados
   
 
   
   if(empty($_GET['data'])){//navegaçao entre os meses
	   $dia = date('d');
	   $month = ltrim(date('m'),"0");
	   $ano = date('Y');
   }else{
	   $data = explode('/',$_GET['data']);//nova data
	   $dia = $data[0];
	   $month = $data[1];
	   $ano = $data[2];
   }
   
   if($month==1){//mês anterior se janeiro mudar valor
	   $mes_ant = 12;
	   $ano_ant = $ano - 1;
   }else{
	   $mes_ant = $month - 1;
	   $ano_ant = $ano;
   }
   
   if($month==12){//proximo mês se dezembro tem que mudar
	   $mes_prox = 1;
	   $ano_prox = $ano + 1;
   }else{
	   $mes_prox = $month + 1;
	   $ano_prox = $ano;
   }
   
   $hoje = date('j');//função importante pego o dia corrente
   switch($month){/*notem duas variaveis para o switch para identificar dia e limitar numero de dias*/
	   case 1: $mes = "JANEIRO";
			   $n = 31;
	   break;
	   case 2: $mes = "FEVEREIRO";// todo ano bixesto fev tem 29 dias
			   $bi = $ano % 4;//anos multiplos de 4 são bixestos
			   if($bi == 0){
				   $n = 29;
			   }else{
				   $n = 28;
			   }
	   break;
	   case 3: $mes = "MARÇO";
			   $n = 31;
	   break;
	   case 4: $mes = "ABRIL";
			   $n = 30;
	   break;
	   case 5: $mes = "MAIO";
			   $n = 31;
	   break;
	   case 6: $mes = "JUNHO";
			   $n = 30;
	   break;
	   case 7: $mes = "JULHO";
			   $n = 31;
	   break;
	   case 8: $mes = "AGOSTO";
			   $n = 31;
	   break;
	   case 9: $mes = "SETEMBRO";
			   $n = 30;
	   break;
	   case 10: $mes = "OUTUBRO";
			   $n = 31;
	   break;
	   case 11: $mes = "NOVEMBRO";
			   $n = 30;
	   break;
	   case 12: $mes = "DEZEMBRO";
			   $n = 31;
	   break;
   }
   
   $pdianu = mktime(0,0,0,$month,1,$ano);//primeiros dias do mes
   $dialet = date('D', $pdianu);//escolhe pelo dia da semana
   switch($dialet){//verifica que dia cai
	   case "Sun": $branco = 0; break;
	   case "Mon": $branco = 1; break;
	   case "Tue": $branco = 2; break;
	   case "Wed": $branco = 3; break;
	   case "Thu": $branco = 4; break;
	   case "Fri": $branco = 5; break;
	   case "Sat": $branco = 6; break;
   }			
   
	   print '<table class="tabela" >';//construção do calendario
	   print '<tr>';
	   print '<td class="mes"><a href="?data='.$dia.'/'.$mes_ant.'/'.$ano_ant.'" title="Mês anterior">  « </a></td>';/*mês anterior*/
	   print '<td class="mes" colspan="5">'.$mes.'/'.$ano.'</td>';/*mes atual e ano*/
	   print '<td class="mes"><a href="?data='.$dia.'/'.$mes_prox.'/'.$ano_prox.'" title="Próximo mês">  » </a></td>';/*Proximo mês*/
	   print '</tr><tr>';
	   print '<td class="sem">D</td>';//printar os dias da semana
	   print '<td class="sem">S</td>';
	   print '<td class="sem">T</td>';
	   print '<td class="sem">Q</td>';
	   print '<td class="sem">Q</td>';
	   print '<td class="sem">S</td>';
	   print '<td class="sem">S</td>';
	   print '</tr><tr>';
	   $dt = 1;
	   if($branco > 0){
		   for($x = 0; $x < $branco; $x++){
				print '<td> </td>';/*preenche os espaços em branco*/
			   $dt++;
		   }
	   }
	   
	   for($i = 1; $i <= $n; $i++ ){/*agora vamos no banco de dados verificar os evendos*/

			   $dtevento = $ano."-".$month."-".$i;
			   $date = new DateTime($dtevento);
			   $data_agenda=$date->format('Y-m-d');
			   $variavel="select * from agenda where data='$data_agenda' and id_medico='$id_medico'";
 			   $sqlag=pg_query($conexao,$variavel);
 			   $num=pg_num_rows($sqlag);		       
			   $registro = pg_fetch_array($sqlag);
			   $idev=$registro['data'];
  
				  if($num > 0){/*prevalece qualquer dia especial do calendario, por isso está em primeiro*/
			  print '<td class="evt">';
			  print '<a href="?d='.$idev.'&data='.$dia.'/'.$month.'/'.$ano.'">'.$i.'</a>';
			  print '</td>';
			  $dt++;/*incrementa os dias da semana*/
		  }elseif($i == $hoje){/*imprime os dia corrente*/
			   print '<td class="hj">';
			   print $i;
			   print '</td>';
			   $dt++;
		   
		   }elseif($dt == 1){/*imprime os domingos*/
			   print '<td class="dom">';
			   print $i;
			   print '</td>';
			   $dt++;
		   }else{/*imprime os dias normais*/
					   print '<td class="td">';
			   print $i;
			   echo $idev;
			   print '</td>';
			   $dt++;
				   }
		   if($dt > 7){/*faz a quebra no sabado*/
		   print '</tr><tr>';
		   $dt = 1;
		   }
	   }
	   print '</tr>';
	   print '</table>';
		  // if($qt > 0){/*se tiver evento no mês imprime quantos tem */
			// print "Temos ".$qt." evento(s) em ".strtolower($mes)."<br>";/*mudar para caixa baixa as letras do mes*/
//		   }
   if(isset($_GET['d'])){/*link dos dias de eventos*/
	   $idev = $_GET['d'];
	     $variavel="select * from agenda inner join paciente using (id_paciente) where data='$idev' and id_medico='$id_medico'";
 			   $sqlag=pg_query($conexao,$variavel);
 			   $num=pg_num_rows($sqlag);		       
print'<br>';
print '<div class="mover">';
 print '<table width="70%" >';/*monta a tabela de eventos*/
  print '<tr><td class="linha"><b>Nome: </b></td>';
   print '<td class="linha"><b>Horario: </b></td></tr>';
	   for($j = 0; $j < $num; $j++){/*caso no mesmo dia tenha mais eventos continua imprimindo */
	   	 $registro = pg_fetch_array($sqlag);
	   $nome = $registro['nome'];/*pegando os valores do banco referente ao evento*/
	   $horario = $registro['horario'];
	   
  
   print '<tr><td class="linha">'.$nome.'</td>';
      print '<td class="linha">'.$horario.'</td><tr>';
 
	   }
	     print '</table>';
	     print '<div>';
   }
   
   ?>
   <br>
  

   </body>
   </html>